export const PermissionType = ['user', 'admin']
export type PermissionType = 'user' | 'admin'

export class User {
    id: string
    firstName: string
    lastName: string
    email: string
    birth: string
    permission: PermissionType
}

