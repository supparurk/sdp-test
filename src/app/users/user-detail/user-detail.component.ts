import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import userData from '../../../assets/datas/users.json';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss']
})
export class UserDetailComponent implements OnInit {

  rawData = userData
  currentId: string

  formGroup: FormGroup

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder
  ) {
    this.route.paramMap.subscribe(params => {
      this.currentId = params.get('id') ? params.get('id') : undefined;
    })
  }

  ngOnInit() {
    const currentData = this.rawData.find(x => x.id == this.currentId)
    this.formGroup = this.fb.group({
      firstName: [currentData ? currentData.firstName : '', Validators.required],
      lastName: [currentData ? currentData.lastName : '', Validators.required],
      email: [currentData ? currentData.email : '', [Validators.required, Validators.email]],
      birth: [currentData ? new Date(currentData.birth) : '', Validators.required],
      permission: [currentData ? currentData.permission : '', Validators.required],
    })

    this.formGroup.valueChanges.subscribe(res => {
      console.log('res', res)
    })


  }


  onClose() {
    this.router.navigate([`users`])
  }


}
