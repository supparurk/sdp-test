import { Component, Input, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { User } from '../../_models/user.model';
import userData from '../../../../assets/datas/users.json';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-table',
  templateUrl: './user-table.component.html',
  styleUrls: ['./user-table.component.scss']
})
export class UserTableComponent implements OnInit {

  rawData = userData

  displayedColumns: string[] = ['id', 'firstName', 'lastName', 'email', 'permission', 'delete'];
  dataSource = new MatTableDataSource(this.rawData)

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onCreate() {
    this.router.navigate([`users/create`])
  }

  onDelete(value) {
    const { id } = value
    const index = this.rawData.findIndex((x: User) => x.id === id)
    this.rawData.splice(index, 1)
    this.dataSource = new MatTableDataSource(this.rawData);
  }
}
