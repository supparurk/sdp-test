import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {

  constructor() { }

  @Output() onClickExpanded = new EventEmitter();
  @Output() onClickLogout = new EventEmitter();


  ngOnInit(): void {
  }

  clickExpanded() {
    this.onClickExpanded.emit();
  }

  clickLogout() {
    this.onClickLogout.emit();
  }


}
