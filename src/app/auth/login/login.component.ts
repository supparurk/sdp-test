import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../auth.service';

interface FormValue {
  email: string
  password: string
}

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public formGroup: FormGroup
  public matcher = new MyErrorStateMatcher()
  public loading = false
  public submitted = false
  public error = ''
  public returnUrl: string;


  get email() {
    return this.formGroup.get('email') as FormControl
  }

  get form() {
    return this.formGroup.controls;
  }

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
  ) {
    this.formGroup = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    })

  }


  async ngOnInit() {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    const isLoggedIn = await this.authService.isLoggedIn()
    // if (isLoggedIn) {
    //   this.router.navigate([`${this.returnUrl}`])
    // }
  }

  async onLogin(formValue: FormValue) {
    this.submitted = true
    const { email, password } = formValue
    if (this.formGroup.invalid) {
      return
    }
    this.router.navigate([`dashboard`])
  }

  toRegister() {
    this.router.navigate([`auth/register`])
  }


}
