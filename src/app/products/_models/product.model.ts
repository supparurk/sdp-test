export const UnitType = ['piece', 'pack']
export type UnitType = 'piece' | 'pack'

export class Products {
    id: string
    productName: string
    price: number
    quantity: number
    unit: UnitType
}