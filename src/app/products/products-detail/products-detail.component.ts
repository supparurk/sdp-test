import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import productData from '../../../assets/datas/products.json';

@Component({
  selector: 'app-products-detail',
  templateUrl: './products-detail.component.html',
  styleUrls: ['./products-detail.component.scss']
})
export class ProductsDetailComponent implements OnInit {

  rawData = productData
  currentId: string

  formGroup: FormGroup

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder
  ) {
    this.route.paramMap.subscribe(params => {
      this.currentId = params.get('id') ? params.get('id') : undefined;
    })
  }

  ngOnInit() {
    // id: string
    // productName: string
    // price: number
    // quantity: number
    // unit: UnitType
    const currentData = this.rawData.find(x => x.id == this.currentId)
    this.formGroup = this.fb.group({
      productName: [currentData ? currentData.productName : '', Validators.required],
      price: [currentData ? currentData.price : '', Validators.required],
      quantity: [currentData ? currentData.quantity : '', [Validators.required, Validators.email]],
      unit: [currentData ? currentData.unit : '', Validators.required],
    })

    this.formGroup.valueChanges.subscribe(res => {
      console.log('res', res)
    })


  }


  onClose() {
    this.router.navigate([`products`])
  }


}
