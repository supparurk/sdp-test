import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import productData from '../../../assets/datas/products.json';
import { Products } from '../_models/product.model';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.scss']
})
export class ProductsListComponent implements OnInit {

  rawData = productData
  // id: string
  // productName: string
  // price: number
  // quantity: number
  displayedColumns: string[] = ['id', 'productName', 'price', 'quantity', 'delete'];
  dataSource = new MatTableDataSource(this.rawData)

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onCreate() {
    this.router.navigate([`products/create`])
  }

  onDelete(value) {
    const { id } = value
    const index = this.rawData.findIndex((x: Products) => x.id === id)
    this.rawData.splice(index, 1)
    this.dataSource = new MatTableDataSource(this.rawData);
  }
}
