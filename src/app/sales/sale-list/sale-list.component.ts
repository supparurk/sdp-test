import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import salesData from '../../../assets/datas/sales.json';
import { Sales } from '../_models/sales.model';

@Component({
  selector: 'app-sale-list',
  templateUrl: './sale-list.component.html',
  styleUrls: ['./sale-list.component.scss']
})
export class SaleListComponent implements OnInit {

  rawData = salesData
  // id: string
  // productName: string
  // price: number
  // quantity: number
  displayedColumns: string[] = ['id', 'firstName', 'lastName', 'position', 'delete'];
  dataSource = new MatTableDataSource(this.rawData)

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onCreate() {
    this.router.navigate([`sales/create`])
  }

  onDelete(value) {
    const { id } = value
    const index = this.rawData.findIndex((x: Sales) => x.id === id)
    this.rawData.splice(index, 1)
    this.dataSource = new MatTableDataSource(this.rawData);
  }

}
