export const PositionType = ['sales', 'manager', 'supervisor', 'defactor']
export type PositionType = 'sales' | 'manager' | 'supervisor' | 'defactor'

export class Sales {
    id: string
    firstName: string
    lastName: string
    sex: string
    birth: string
    position: PositionType
}