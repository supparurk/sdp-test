import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SaleDetailComponent } from './sale-detail/sale-detail.component';
import { SaleListComponent } from './sale-list/sale-list.component';


const routes: Routes = [
    {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full',
    },
    {
        path: 'list',
        component: SaleListComponent
    },
    {
        path: 'detail/:id',
        component: SaleDetailComponent
    },
    {
        path: 'create',
        component: SaleDetailComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SalesRoutingModule { }
